Original code
-------------

http://code.google.com/p/cobaltojs/source/browse/trunk/co-validation.js?r=2


Changes
-------

- Used alerts classes from twitter bootstrap
- Removed code, that show field title instead of error message if title exists (I catch some errors where)
- Error message now vieved before field instead of after
- added ability to specify callback function on_success
